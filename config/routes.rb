Rails.application.routes.draw do
  resources :lists do
    resource :cards
  end

  resources :tags do
    resource :cards
  end

  resources :cards do
    resource :lists
    resource :tags
    post '/tags/:id', to: 'tags#create'
  end

  mount_devise_token_auth_for 'User', at: 'auth'

  resources :boards do
    resource :users
    resource :tags
    resource :lists
  end

  resources :listings

  resources :taggings

  resources :creations

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
