class ListingsController < ApplicationController
  # Para realizar os testes, comente a linha 'before_action :authenticate_user!
  # Se não nenhum teste que a realização requisição será aprovado e assim, o
  # teste irá falhar.
  before_action :authenticate_user!
  before_action :set_listing, only: [:show, :update, :destroy]

  # GET /listings
  def index
    @listings = Listing.all

    render json: @listings
  end

  # GET /listings/1
  def show
    render json: @listing
  end

  # POST /listings
  def create
    @listing = Listing.new(listing_params)

    if @listing.save
      render json: @listing, status: :created, location: @listing
    else
      render json: @listing.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /listings/1
  def update
    if @listing != "listing not found!"
      if @listing.update(listing_params)
        render json: @listing
      else
        render json: @listing.errors, status: :unprocessable_entity
      end
    else
      render json: @listing
    end
  end

  # DELETE /listings/1
  def destroy
    if @listing != "listing not found!"
      @listing.destroy
    else
      render json: @listing
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
      @listing = Listing.find_by_id(params[:id])
      if !@listing
        @listing = "listing not found!"
      end
    end

    # Only allow a trusted parameter "white list" through.
    def listing_params
      params.permit(:list_id, :card_id)
    end
end
