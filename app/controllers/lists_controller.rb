class ListsController < ApplicationController
  # Para realizar os testes, comente a linha 'before_action :authenticate_user!
  # Se não nenhum teste que a realização requisição será aprovado e assim, o
  # teste irá falhar.
  before_action :authenticate_user!
  before_action :set_list, only: [:show, :update, :destroy]

  # GET /lists
  def index
    @lists = List.all

    render json: @lists
  end

  # GET /lists/1
  def show
    render json: @list
  end

  # POST /lists
  def create
    @list = List.new(list_params)

    if @list.save
      render json: @list, status: :created, location: @list
    else
      render json: @list.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /lists/1
  def update
    if @list != "List not found!" 
      if @list.update(list_params)
        render json: @list
      else
        render json: @list.errors, status: :unprocessable_entity
      end
    else 
      render json: @list
    end
  end

  # DELETE /lists/1
  def destroy
    if @list != "List not found!"
      @list.destroy
    else
      render json: @list
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      if params[:card_id]
        @list = Card.find(params[:card_id]).lists
        return
      end

      if params[:board_id]
        @list = Board.find(params[:board_id]).lists
        return
      end

      @list = List.find_by_id(params[:id])
      if !@list 
        @list = "List not found!"
      end
    end

    # Only allow a trusted parameter "white list" through.
    def list_params
      params.require(:list).permit(:title, :color, :sort, :board_id)
    end
end
