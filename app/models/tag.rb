class Tag < ApplicationRecord
  belongs_to :board

  has_many :taggings, :dependent => :destroy
  has_many :cards, :through => :taggings
end
