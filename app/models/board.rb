class Board < ApplicationRecord
    has_many :lists, :dependent => :destroy
    has_many :tags, :dependent => :destroy

    has_many :creations, :dependent => :destroy
    has_many :users, :through => :creations
end
