class List < ApplicationRecord
  belongs_to :board

  has_many :listings, :dependent => :destroy
  has_many :cards, :through => :listings
end
