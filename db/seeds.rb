# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
board = Board.create({ :title => 'Board', :color => '#FAFAFA', :visualization => 'kanban', :period => 'weekly' })

tag0 = Tag.create({ :title => 'Tag1', :color => '#F3F3F3', :board => board })
tag1 = Tag.create({ :title => 'Tag2', :color => '#F3F3F3', :board => board })

list1 = List.create({ :title => 'List1', :color => '#F0F0F0', :sort => 'priority', :board => board })
list2 = List.create({ :title => 'List2', :color => '#F0F0F0', :sort => 'priority', :board => board })

card0 = Card.create({ :title => 'Seeds', :color => '#FFFFFF', :priority => 1, :date => "19/05/2018 23:55"})
card1 = Card.create({ :title => 'Entidades', :color => '#FFFFFF', :priority => 2, :date => "19/05/2018 23:55"})
card2 = Card.create({ :title => 'Conexão', :color => '#FFFFFF', :priority => 3, :date => "19/05/2018 23:55"})

card0.taggings.create({ :tag => tag0 })
card0.taggings.create({ :tag => tag1 })

card1.taggings.create({ :tag => tag0 })
card2.taggings.create({ :tag => tag1 })

list1.listings.create({ :card => card0 })
list1.listings.create({ :card => card1 })
list2.listings.create({ :card => card2 })
