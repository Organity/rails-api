class CreateListings < ActiveRecord::Migration[5.1]
  def change
    create_table :listings do |t|
      t.references :list, foreign_key: true
      t.references :card, foreign_key: true

      t.timestamps
    end
  end
end
