require 'rails_helper'

###################################
#
# Acho que não precisa mais do seed
# O seed é pra default values
# Precisa arrumar o delete cascade
#
###################################

RSpec.describe BoardsController, type: :controller do
    before :all do
        @initialLength = Board.all.length
        @board = create(:board)
        @jsonBoard = {
            "title" => "New",
            "color" => "Azulzinho",
            "period" => "monthly",
            "visualization" => "custom"
        }
    end

    after :all do 
        @board.destroy
    end

    describe "GET #index" do
        it "returns all boards" do
            get :index
            body = JSON.parse(response.body)
            expect(body.length).to eq(@initialLength + 1)
        end
    end

    describe "GET #show" do
        it "returns board with the given index" do
            get :show, params: {id: @board.id}
            body = JSON.parse(response.body)
            expect(body["id"]).to eq(@board.id)
        end

        
        it "return a error with wrong index to show" do
            get :show, params: {id:1000000}
            expect(response.body).to eq("Board not found!")
        end
    end

    describe "POST #create" do
        it "creates a new board in data base" do
            jsonBoard = {
                "title" => @board.title,
                "color" => @board.color,
                "period" => @board.period,
                "visualization" => @board.visualization
            }

            expect{
                post :create, params: { board: jsonBoard}
            }.to change(Board, :count).by(1)
        end
    end

    describe "PUT #update" do
        it "updates a board" do
            jsonBoard = {
                "title" => "New",
                "color" => "Azulzinho",
                "period" => "monthly",
                "visualization" => "custom"
            }
            put :update, params: {id: @board.id, board: @jsonBoard}
            body = JSON.parse(response.body)
            expect(body["color"]).to eq("Azulzinho")
        end
        
        it "return a error with wrong index to update" do
            resp = put :update, params: {id: 1000000, board: @jsonBoard}
            expect(resp.body).to eq("Board not found!")
        end
    end

    describe "DELETE #destroy" do
        it "delete board with given index" do
            expect{
                delete :destroy, params: {id:@board.id}
            }.to change(Board, :count).by(-1)
        end
    end

    
        it "return a error with wrong index to destroy" do
            resp = delete :destroy, params: {id:1000000}
            expect(resp.body).to eq("Board not found!")
        end
end