require 'rails_helper'

###################################
#
# Acho que não precisa mais do seed
# O seed é pra default values
# Precisa arrumar o delete cascade
#
###################################

RSpec.describe TaggingsController, type: :controller do
    before :all do
        @initialLength = Tagging.all.length
        @board = create(:board)
        @card = create(:card)
        @tag = create(:tag, board: @board)
        @tagging = create(:tagging, card: @card, tag: @tag)
        
        @jsonTagging = {
            "card_id" => @tagging.card.id,
            "tag_id" => @tagging.tag.id
        }
    end

    after :all do 
        @tagging.destroy
        @card.destroy
        @tag.destroy
        @board.destroy
    end

    describe "GET #index" do
        it "returns all taggings" do
            get :index
            body = JSON.parse(response.body)
            expect(body.length).to eq(@initialLength + 1)
        end
    end

    describe "GET #show" do
        it "returns tagging with the given index" do
            get :show, params: {id: @tagging.id}
            body = JSON.parse(response.body)
            expect(body["id"]).to eq(@tagging.id)
        end

        
        it "return a error with wrong index to show" do
            get :show, params: {id:1000000}
            expect(response.body).to eq("tagging not found!")
        end
    end

    describe "POST #create" do
        it "creates a new tagging in data base" do
            
            jsonTagging = {
                "card_id" => @tagging.card.id,
                "tag_id" => @tagging.tag.id
            }
            expect{
                post :create, params: { tagging: jsonTagging}
            }.to change(Tagging, :count).by(1)
        end
    end

    describe "PUT #update" do
        it "updates a tagging" do
            put :update, params: {id: @tagging.id, tagging: @jsonTagging}
            body = JSON.parse(response.body)
            expect(body["card_id"]).to eq(@tagging.card.id)
        end
        
        it "return a error with wrong index to update" do
            resp = put :update, params: {id: 1000000, tagging: @jsonTagging}
            expect(resp.body).to eq("tagging not found!")
        end
    end

    describe "DELETE #destroy" do
        it "delete tagging with given index" do
            expect{
                delete :destroy, params: {id:@tagging.id}
            }.to change(Tagging, :count).by(-1)
        end
    end

    
        it "return a error with wrong index to destroy" do
            resp = delete :destroy, params: {id:1000000}
            expect(resp.body).to eq("tagging not found!")
        end
end
