FactoryBot.define do
    factory :board, class: Board do
        title           "Board"
        color           "Ultravioleta"
        visualization   "kanban"
        period          "weekly"
    end
end