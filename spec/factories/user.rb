FactoryBot.define do
    factory :user, class: User do
        name       "João"
        email      "Jo@o.com"
        password   "12345678"
    end
end