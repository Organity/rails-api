require 'rails_helper'

RSpec.describe Board, type: :model do
    before :all do
        @board = create(:board)
        @invalid = build(:board, id: @board.id)
    end
   
    after :all do
        @board.destroy
    end

    it 'must be a valid with valid attributes' do
        expect(@board).to be_valid
    end

    it 'must be invalid with same id' do
        expect{ @invalid.save! }.to raise_error( ActiveRecord::RecordNotUnique )
    end
end 
